var app = angular.module('Manchkin', ['ngRoute']);


//var routeSegmentProvider = null;

//function Game($scope, e) {
//    console.log(e);
//}

app.config(['$routeProvider', function($routeProvider) {
   $routeProvider
       .when('/', {
            controller: 'login',
            templateUrl: 'login.html'

       })
       .when('/waiting_room', {
           controller: 'waitingRoom',
           templateUrl: 'waitingRoom.html'

       })
       .when('/game', {
           controller: 'gameCtrl',
           templateUrl: 'gg.html',
           //resolve: {
           //    e: ['preloader', function (preloader) {console.log('api') ;return preloader}]
           //}
           resolve: {
               owner: ['api', function (api) {return api.getOwner()}],
               players: ['api', function (api) {return api.getOtherPlayers()}]
           }
       })

}]);

//app.run(['$http', function($http) {
//    routeSegmentProvider.
//        when('/', {
//            controller: 'Game',
//            resolve: {e: function() {console.log('uh');return $http.get('/api/v1/players')}},
//            resolveAs: 'aaaaaaa'
//        })
//}    ]);
app.controller('login', ['$scope', function($scope) {

}]);

app.controller('waitingRoom', ['$scope', function($scope) {

}]);


app.controller('gameCtrl', ['$scope', 'owner', 'players', function($scope, owner, players) {
    $scope.owner = owner;
    $scope.players = players;
}]);


app.directive('loading', ['$http', function ($http) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };

            scope.$watch(scope.isLoading, function (v) {
                if(v) {
                    $(elm).show();
                } else {
                    $(elm).hide();
                }

            })
        }
    }
}]);

app.directive('rotate', function() {
	return {
		link: function(scope, element, attrs) {
			// watch the degrees attribute, and update the UI when it changes
			scope.$watch(attrs.degrees, function(rotateDegrees) {
				//console.log(rotateDegrees);
				//transform the css to rotate based on the new rotateDegrees
				element.css({
					'-moz-transform': 'rotate(' + rotateDegrees + 'rad)',
					'-webkit-transform': 'rotate(' + rotateDegrees + 'rad)',
					'-o-transform': 'rotate(' + rotateDegrees + 'rad)',
					'-ms-transform': 'rotate(' + rotateDegrees + 'rad)'
				});
			});
		}
	}
});


app.directive('draggable', function() {
	return function(scope, element) {
		var el = element[0];

		el.draggable = true;

		el.addEventListener(
			'dragstart',
			function(e) {
				e.dataTransfer.effectAllowed = 'move';
				e.dataTransfer.setData('Text', this.id);
				this.classList.add('drag');
				return false;
			},
			false
		);

		el.addEventListener(
			'dragend',
			function(e) {
				this.classList.remove('drag');
				return false;
			},
			false
		);
	}
});


app.directive('droppable', function() {
	return {
		scope: {
			drop: '&', // parent
		  bin: '=' // bi-directional scope	
		},
		link: function(scope, element) {
			var el = element[0];

			el.addEventListener(
		    'dragover',
		    function(e) {
		        e.dataTransfer.dropEffect = 'move';
		        // allows us to drop
		        if (e.preventDefault) e.preventDefault();
		        this.classList.add('over');
		        return false;
			  },
			  false
			);

			el.addEventListener(
			    'dragleave',
			    function(e) {
			        this.classList.remove('over');
			        return false;
			    },
			    false
			);

			el.addEventListener(
        'drop',
        function(e) {
          // Stops some browsers from redirecting.
          if (e.stopPropagation) e.stopPropagation();
          
          this.classList.remove('over');
          
          var binId = this.id;
          var item = document.getElementById(e.dataTransfer.getData('Text'));
          // this.appendChild(item);
          // call the passed drop function
          scope.$apply(function(scope) {
            var fn = scope.drop();
            if ('undefined' !== typeof fn) {
              fn(item.id, binId);
            }
          });
          
          return false;
        },
        false
      );



		}
	}
})