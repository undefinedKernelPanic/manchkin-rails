# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class RoomChannel < ApplicationCable::Channel
  @@players = []
  def subscribed
    stream_from "room_channel"
    # @@players.push params[:user]
  end

  def unsubscribed
    p 'server'
    # current_user = nil
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
    Message.create! content: data['message']
    # ActionCable.server.broadcast 'room_channel', message: data['message']
  end

  def away
    current_user.away
    p 'away'
  end
end