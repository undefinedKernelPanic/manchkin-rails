class Room
  attr :players

  def add_player(player)
    @players.push player
  end

  def remove_player(player)
    @players.delete player
  end
end