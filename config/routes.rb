Rails.application.routes.draw do
  root to: 'application#angular'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  mount ActionCable.server => '/cable'

  namespace :api do
    namespace :v1 do
      get 'players', to: 'players#index'
      get 'other_players', to: 'players#other_players'
    end
  end

end
