class Player
  def initialize(name)
    @name = name
    @level = 1
    @power = 1
    @hand = []
    @carried = []
    @equipped = []
  end

  def add_cards(*cards)
    @hand.push cards
  end
end