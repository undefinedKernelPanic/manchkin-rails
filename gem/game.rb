class Game
  # class Dice
  #   def initialize
  #     @dice = Random.new
  #   end
  #
  #   def throw
  #     @dice.rand 1..6
  #   end
  # end

  def initialize(deck, players)
    @deck = deck
    @players = players
    @current_player = nil
    @dice = Random.new
  end

  def start_game
    @deck.shuffle!

    @players.each do |p|
      p.add_cards @deck.draw_start_hand
    end

    @current_player = @players[@dice.rand 0...@players.size]
  end
end