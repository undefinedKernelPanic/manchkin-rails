require 'card'

module ClassicDeck
  def get_doors
    [
        Monster.new(
                   'tmp', 1
        ),
        Monster.new(
                   'aa', 2
        ),
        Race.new
    ]
  end

  def get_treasures
    [
        Gear.new, LevelUp.new, Potion.new
    ]
  end
end