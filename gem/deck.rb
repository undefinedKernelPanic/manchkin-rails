require 'classic_deck'

class Deck
  include ClassicDeck

  def initialize(type = :classic)
    # p type
    @doors = get_doors
    @treasures = get_treasures

    @doors_discard = []
    @treasures_discard = []
  end

  def shuffle!
    @doors.shuffle!
    @treasures.shuffle!
    # deck = args.select { |i| i == :door || i == :treasure }.first || :door
  end

  def draw_start_hand
    [
        draw_card,
        draw_card,
        draw_card(:treasure),
        draw_card(:treasure)
    ]
  end

  def draw_card(*args)
    deck = args.select { |i| i == :door || i == :treasure }.first || :door
    case deck
      when :door
        return @doors.shift
      when :treasure
        return @treasures.shift
    end
  end
end